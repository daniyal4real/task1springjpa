package kz.aitu.project.controllers;


import kz.aitu.project.entity.Student;
import kz.aitu.project.repository.StudentRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StudentController {
    private StudentRepository studentRepository;

    public StudentController(StudentRepository studentRepository){
        this.studentRepository = studentRepository;
    }

    @GetMapping("/students")
    public ResponseEntity<?> getAllStudents(){
        return ResponseEntity.ok(studentRepository.findAll());
    }

    @GetMapping("/students/{id}")
    public ResponseEntity<List<Student>> getStudentById(@PathVariable int id){
        return ResponseEntity.ok(studentRepository.findStudentById(id));
    }

    @GetMapping("/students/all")
    public List<Student> getAll(){
        return studentRepository.ownFindAll();
    }
}
