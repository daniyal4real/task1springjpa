package kz.aitu.project.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="student")
public class Student {
    @Id
    @Column(name = "studentid")
    private int studentId;

    @Column(name = "studentname")
    private String studentName;

    @Column(name = "studentphone")
    private String studentPhone;

    @Column(name = "groupid")
    private int groupId;

    @Override
    public String toString() {
        return "Group id: " + groupId + "   Student id: " + studentId + "   Name: " + studentName +
                "   Phone: " + studentPhone ;
    }
}